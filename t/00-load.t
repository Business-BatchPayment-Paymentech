#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::BatchPayment::Paymentech' ) || print "Bail out!\n";
}

diag( "Testing Business::BatchPayment::Paymentech $Business::BatchPayment::Paymentech::VERSION, Perl $], $^X" );
